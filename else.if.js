// There are still too many sheep for the fragile Death Valley ecosystem. 
// The Rangers would like to make some changes to the population reduction plan.

// Create an if block that checks when monthNumber is a multiple of 4. Inside this block, 
// calculate 75% of the sheep population and store the result in the reduction variable.

// Still inside the if block (after the line reassigning reduction), 
// console.log a message with the format below:
// Removing number sheep from the population.
// Replace number with the actual number of sheep being reduced.

// Lastly inside the if block, subtract the value of reduction from the total number 
// of sheep and store the result back on the numSheep variable.

// Create an else if block that checks when numSheep is greater than 10000. Inside this block, 
// calculate 50% of the sheep population and store the result in the reduction variable.

// Still inside the else if block (after the line reassigning reduction), 
// console.log a message with the format below:
// Removing number sheep from the population.
// Replace number with the actual number of sheep being reduced.


// Lastly inside the else if block, subtract the value of reduction from the 
// total number of sheep and store the result back on the numSheep variable.

let numSheep = 4;
let monthsToPrint = 12;
let reduction;

for (let monthNumber = 1; monthNumber <= monthsToPrint; monthNumber++) {	
  
  // if statement goes here...
    if(monthNumber % 4 == 0){
        let reduction = numSheep * 0.75;
        console.log(`Removing ${reduction} sheep from the population.`);
        numSheep = numSheep - reduction;
    }

  // else if statement goes here...
  else if(numSheep > 10000){
    let reduction = numSheep * 0.50;
    console.log(`Removing ${reduction} sheep from the population.`);
    numSheep = numSheep - reduction;
  }

  numSheep *= 4;
  console.log("There will be " + numSheep + " sheep after " + monthNumber + " month(s)!");
}