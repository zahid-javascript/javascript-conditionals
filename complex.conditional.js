// Our friends at the Hoover Dam called us back and would like 
// a program that simulates what happens when only the even numbered turbines are turned on. 
// They've decided they want it all in just one for loop.

// Let's start by adding an 
// if statement that runs when currentGen is an even number and is less than or equal to 4.

// Inside our if statement, let's increment our totalMW by 62. 
// Then, use the variables currentGen and totalMW to console.log some output. 
// Here are two sample lines from the output. Ensure the format matches:
// Generator #2 is on, adding 62 MW, for a total of 62 MW!
// Generator #4 is on, adding 62 MW, for a total of 124 MW!

// Next, let's add an else if statement that runs when currentGen
//  is an even number and is greater than or equal to 5.

// Inside the else if statement, increment totalMW by 124. 
// Then, use the variables currentGen and totalMW to console.log some output. 
// Here are two sample lines from the output. Ensure the format matches:
// Generator #6 is on, adding 124 MW, for a total of 248 MW!
// Generator #8 is on, adding 124 MW, for a total of 372 MW!

// Finally, add an else statement to account for any odd number generators and 
// console.log some output. Here are two smaple lines from the output. Ensure the format matches:
// Generator #1 is off.
// Generator #3 is off.

let totalGen = 19;
let totalMW = 0; 

for (let currentGen = 1; currentGen <= totalGen; currentGen++) {
    if(currentGen % 2 == 0 && currentGen  <=  4) {
        totalMW += 62;
        console.log(`Generator #  ${currentGen}  is on, adding 62 MW, for a total of ${totalMW}  MW!`);
    }
    else if(currentGen % 2 == 0 && currentGen  >=  5){
        totalMW += 124;
        console.log(`Generator #  ${currentGen}  is on, adding 124 MW, for a total of ${totalMW}  MW!`);
    }
    else {
        console.log(`Generator #  ${currentGen}  is off.`);
    }
	  
}
