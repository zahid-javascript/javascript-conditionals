// Back at Death Valley, scientists could see that the sheep population 
// would quickly grow out of control. They have decided that, 
// for any month the population climbs above 10000, 
// half of the sheep will be sent away to other regions.

// Add an if statement inside the loop that is true when numSheep is greater than 10000. 
// Let's be sure to insert this line of code before multiplying our numSheep varaible by 4.

// Now, inside the if statement, set numSheep equal to half of the total number of sheep.

// Finally, still inside the if statement (after the line reassigning numSheep), 
// print a message to the console with the following format:
// Removing number sheep from the population. 
// Replace number with the actual number of sheep being incremented.

let numSheep = 4;
let monthsToPrint = 12;

for (let monthNumber = 1; monthNumber <= monthsToPrint; monthNumber++) {

  if (numSheep > 10000) {
    numSheep = numSheep / 2;
    console.log("Removing " + numSheep + " sheep from the population.");
  }

  numSheep *= 4;
  console.log("There will be " + numSheep + " sheep after " + monthNumber + " month(s)!");
}