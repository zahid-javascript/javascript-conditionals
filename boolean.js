// Using Boolean Flags in Conditionals
// The Badlands National Park would like to print specific messages depending on 
// whether the park is open or closed.

// Use an if statement to check if the parkIsOpen variable is true. 
// If so, use console.log() to print the following message:
// Welcome to the Badlands National Park! Try to enjoy your stay.

let parkIsOpen = true;
if(parkIsOpen){
    console.log("Welcome to the Badlands National Park! Try to enjoy your stay");
}
// Use an if statement to check if the parkIsOpen variable is true.
// If so, use console.log() to print the following message:
// Welcome to the Badlands National Park! Try to enjoy your stay.
// Next, add an else statement that prints:
// Sorry, the Badlands are particularly bad today. We're closed!

else{
    console.log("Sorry, the Badlands are particularly bad today. We're closed!");
}