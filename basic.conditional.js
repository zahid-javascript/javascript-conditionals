// Let's start learning conditionals by practicing with some simple tasks.

// Add a conditional inside the while loop which will only allow even numbers to be printed.

let num = 10;

while (num > 0) {
    if(num % 2 == 0){
        console.log(num);
    }
  num--;
}
